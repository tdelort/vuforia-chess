# Vuforia Chess

## Targets

There is 2 Targets in the ToPrint folder that you need to print in A4 format.

The stone pattern is for the chess board and the "FISSURE" card is the controller.

## How to play

You will need to use the smaller image target to grab and drop the pieces. 
You will see a red ball attached to the card whith which you can grab the pieces by staying in them from 2 seconds.

## Best Practices

The smaller target works better if there is some margin around it to grab it without covering the image. 
Also, paper tends to bend and it works bettre if it is flat, so try to use thicker paper or double it with cardboard.
Finally, try to avoid glossy paper since the specular lightning will infer with the recognition.