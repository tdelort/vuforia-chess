﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PieceType
{
    PAWN,
    ROOK,
    BISHOP,
    KNIGHT,
    QUEEN,
    KING
}

public class PieceFactory : MonoBehaviour
{

    [SerializeField] PawnBehaviour pawnPrefab;
    [SerializeField] BishopBehaviour bishopPrefab;
    [SerializeField] RookBehaviour rookPrefab;
    [SerializeField] KnightBehaviour knightPrefab;
    [SerializeField] KingBehaviour kingPrefab;
    [SerializeField] QueenBehaviour queenPrefab;

    [SerializeField] Material whiteMaterial;
    [SerializeField] Material blackMaterial;

    [SerializeField] Transform parent;

    public PieceBehaviour Get((int,int) posistion,PieceType type,PieceColor color)
    {
        //Debug.Log(posistion.Item1 + posistion.Item2 + type.ToString() + color.ToString());
        PieceBehaviour piece;
        switch (type)
        {
            case PieceType.PAWN:
                piece = Instantiate<PieceBehaviour>(pawnPrefab, parent);
                break;
            case PieceType.ROOK:
                piece = Instantiate<PieceBehaviour>(rookPrefab, parent);
                break;
            case PieceType.BISHOP:
                piece = Instantiate<PieceBehaviour>(bishopPrefab, parent);
                break;
            case PieceType.KNIGHT:
                piece = Instantiate<PieceBehaviour>(knightPrefab, parent);
                break;
            case PieceType.QUEEN:
                piece = Instantiate<PieceBehaviour>(queenPrefab, parent);
                break;
            case PieceType.KING:
                piece = Instantiate<PieceBehaviour>(kingPrefab, parent);
                break;
            default:
                Debug.LogError("Should never reach here");
                piece = Instantiate<PieceBehaviour>(pawnPrefab, parent);
                break;
        }
        piece.color = color;
        if(color == PieceColor.WHITE)
            piece.GetComponentInChildren<MeshRenderer>().material = whiteMaterial;
        else
            piece.GetComponentInChildren<MeshRenderer>().material = blackMaterial;

        piece.position = new Vector2Int(posistion.Item1, posistion.Item2);
        piece.transform.position = new Vector3(posistion.Item1 * parent.localScale.x, 0f, posistion.Item2 * parent.localScale.z);
        return piece;
    }

}
