﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PieceColor
{
    BLACK,
    WHITE
}

public class PieceBehaviour : MonoBehaviour
{
    protected BoardBehaviour board;
    public PieceColor color { get; set; }

    public Vector2Int position;

    void Start()
    {
        board = GameObject.FindObjectOfType<BoardBehaviour>();
    }

    public virtual List<Vector2Int> PossibleMoves()
    {
        return new List<Vector2Int>();
    }

    public virtual void UpdatePosition(Vector2Int newPosition)
    {
        //board.MovePiece(position, newPosition);
        position = newPosition;
    }

    //on clicked
    //send PossibleMoves to board to highlight them
    private void OnMouseDown()
    {
        board.pieceSelected = this;
        board.HighlightPossibleMoves();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Hand"))
        {
            //board.pieceSelected = this;
            Debug.Log("triggered");
            //board.HidePossibleMoves();
        }
    }
}
