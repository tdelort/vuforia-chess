﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandControllerKeyboard : MonoBehaviour
{
    float horizontalInput;
    float verticalInput;
    bool up = true;

    // Input management
    void Update()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        if (Input.GetKeyDown("right ctrl") && up)
        {
            gameObject.transform.Translate(new Vector3(0, -2, 0));
            up = false;
        }
        if (Input.GetKeyDown("right shift") && !up)
        {
            gameObject.transform.Translate(new Vector3(0, 2, 0));
            up = true;
        }
        
        if (Input.GetKeyDown("up"))
        {
            if (gameObject.transform.position.x == 0)
                return;
            gameObject.transform.Translate(new Vector3(-1, 0, 0));
        }
        if (Input.GetKeyDown("down"))
        {
            if (gameObject.transform.position.x == 7)
                return;
            gameObject.transform.Translate(new Vector3(1, 0, 0));
        }
        if (Input.GetKeyDown("left"))
        {
            if (gameObject.transform.position.z == 0)
                return;
            gameObject.transform.Translate(new Vector3(0, 0, -1));
        }
        if (Input.GetKeyDown("right"))
        {
            if (gameObject.transform.position.z == 7)
                return;
            gameObject.transform.Translate(new Vector3(0, 0, 1));
        }
    }

   
}
