﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnightBehaviour : PieceBehaviour
{

    List<Vector2Int> OneMove(List<Vector2Int> moves, Vector2Int queryPosition)
    {
        if (queryPosition.x >= 8 || queryPosition.y >= 8 || queryPosition.x < 0 || queryPosition.y < 0)
            return moves;
        bool oc = board.Occupied(queryPosition, out PieceColor otherColor);
        if(!oc)
        {
            moves.Add(queryPosition);
        }
        else
        {
            if (otherColor != color)
                moves.Add(queryPosition);
        }
        return moves;
    }

    public override List<Vector2Int> PossibleMoves()
    {
        // . o . o .  (+1,+2) (+2,+1)
        // o . . . o  (+2,-1) (+1,-2)
        // . . . . .  (-1,-2) (-2,-1)
        // o . . . o  (-2,+1) (-1,+2)
        // . o . o .
        var moves = new List<Vector2Int>();

        moves = OneMove(moves, new Vector2Int(position.x + 1, position.y + 2)); 
        moves = OneMove(moves, new Vector2Int(position.x + 2, position.y + 1));
        moves = OneMove(moves, new Vector2Int(position.x + 2, position.y + -1));
        moves = OneMove(moves, new Vector2Int(position.x + 1, position.y + -2));

        moves = OneMove(moves, new Vector2Int(position.x + -1, position.y + -2));
        moves = OneMove(moves, new Vector2Int(position.x + -2, position.y + -1));
        moves = OneMove(moves, new Vector2Int(position.x + -2, position.y + 1));
        moves = OneMove(moves, new Vector2Int(position.x + -1, position.y + 2));

        return moves;
    }
}
