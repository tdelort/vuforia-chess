﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KingBehaviour : PieceBehaviour
{
    public override List<Vector2Int> PossibleMoves()
    {
        var moves = new List<Vector2Int>();

        for(int y = position.y - 1; y <= position.y + 1; y++)
        {
            for(int x = position.x - 1; x <= position.x + 1; x++)
            {
                if (x == position.x && y == position.y)
                    continue;


                Vector2Int queryPosition = new Vector2Int(x, y);
                if (queryPosition.x >= 8 || queryPosition.y >= 8 || queryPosition.x < 0 || queryPosition.y < 0)
                    continue;

                bool oc = board.Occupied(queryPosition, out PieceColor otherColor);
                if(!oc)
                {
                    moves.Add(queryPosition);
                }
                else
                {
                    if (otherColor != color)
                        moves.Add(queryPosition);
                }
            }
        }

        return moves;
    }
}
