﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueenBehaviour : PieceBehaviour
{

    public override List<Vector2Int> PossibleMoves()
    {
        var bishopMoves = BishopBehaviour.BishopMoves(position, color, board);
        var rookMoves = RookBehaviour.RookMoves(position, color, board);
        bishopMoves.AddRange(rookMoves);
        return bishopMoves;
    }
}
