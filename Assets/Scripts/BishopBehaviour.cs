﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BishopBehaviour : PieceBehaviour
{
    public static List<Vector2Int> BishopMoves(Vector2Int from, PieceColor color, BoardBehaviour board)
    {
        var moves = new List<Vector2Int>();
        // →↑
        for (Vector2Int pos = new Vector2Int(from.x + 1, from.y + 1); pos.x < 8 && pos.y < 8; pos.x++, pos.y++)
        {
            bool oc = board.Occupied(pos, out PieceColor otherColor);
            if(!oc)
            {
                moves.Add(pos);
            }
            else
            {
                if (otherColor != color)
                    moves.Add(pos);
                break;
            }
        }
        // →↓
        for (Vector2Int pos = new Vector2Int(from.x + 1, from.y - 1); pos.x < 8 && pos.y >= 0; pos.x++, pos.y--)
        {
            bool oc = board.Occupied(pos, out PieceColor otherColor);
            if(!oc)
            {
                moves.Add(pos);
            }
            else
            {
                if (otherColor != color)
                    moves.Add(pos);
                break;
            }
        }

        // ↓←
        for (Vector2Int pos = new Vector2Int(from.x - 1, from.y - 1); pos.x >= 0 && pos.y >= 0; pos.x--, pos.y--)
        {
            bool oc = board.Occupied(pos, out PieceColor otherColor);
            if(!oc)
            {
                moves.Add(pos);
            }
            else
            {
                if (otherColor != color)
                    moves.Add(pos);
                break;
            }
        }
        // ↑←
        for (Vector2Int pos = new Vector2Int(from.x - 1, from.y + 1); pos.x >= 0 && pos.y < 8; pos.x--, pos.y++)
        {
            bool oc = board.Occupied(pos, out PieceColor otherColor);
            if(!oc)
            {
                moves.Add(pos);
            }
            else
            {
                if (otherColor != color)
                    moves.Add(pos);
                break;
            }
        }
        return moves;
    }

    public override List<Vector2Int> PossibleMoves()
    {
        return BishopMoves(position, color, board);
    }
}
