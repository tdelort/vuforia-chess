﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnBehaviour : PieceBehaviour
{
    bool firstMove = true;

    List<Vector2Int> OneMove(List<Vector2Int> moves, Vector2Int queryPosition)
    {
        if (queryPosition.x >= 8 || queryPosition.y >= 8 || queryPosition.x < 0 || queryPosition.y < 0)
            return moves;
        bool oc = board.Occupied(queryPosition, out PieceColor otherColor);
        if(!oc)
        {
            moves.Add(queryPosition);
        }
        else
        {
            if (otherColor != color)
                moves.Add(queryPosition);
        }
        return moves;
    }

    public override List<Vector2Int> PossibleMoves()
    {
        Vector2Int direction = (color == PieceColor.BLACK ? Vector2Int.up : Vector2Int.down);
        var moves = new List<Vector2Int>();

        if(!board.Occupied(position + direction, out PieceColor _))
            moves = OneMove(moves, position + direction);

        if(firstMove && moves.Count > 0) //Stop it from jumping over pieces
            moves = OneMove(moves, position + direction * 2);

        if(board.Occupied(position + direction + Vector2Int.right, out PieceColor rightColor) && rightColor != color)
            moves = OneMove(moves, position + direction + Vector2Int.right);

        if(board.Occupied(position + direction + Vector2Int.left, out PieceColor leftColor) && leftColor != color)
            moves = OneMove(moves, position + direction + Vector2Int.left);

        return moves;
    }

    public override void UpdatePosition(Vector2Int newPosition)
    {
        base.UpdatePosition(newPosition);
        firstMove = false;
    }
}
