﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardBehaviour : MonoBehaviour
{
    GameBehaviour game ; //reference needed in SetCurrentColliding()
    [SerializeField] SquareBehaviour squarePrefab;

    public Dictionary<Vector2Int, PieceBehaviour> board = new Dictionary<Vector2Int, PieceBehaviour>();
    public SquareBehaviour[,] squares = new SquareBehaviour[8, 8];
    //PieceBehaviour[,] board = new PieceBehaviour[8,8];

    public PieceBehaviour pieceSelected;
    public Vector2Int currentCollidingSquare;

    float timeOfDeplacement;

    private void Start()
    {
        InitBoard();
        InitBoardSquares();
        game = FindObjectOfType<GameBehaviour>();
    }

    void InitBoard()
    {
        PieceFactory factory = GameObject.FindObjectOfType<PieceFactory>();
        board.Add(new Vector2Int(0, 0), factory.Get((0,0), PieceType.ROOK, PieceColor.BLACK));
        board.Add(new Vector2Int(1, 0), factory.Get((1,0), PieceType.KNIGHT, PieceColor.BLACK));
        board.Add(new Vector2Int(2, 0), factory.Get((2,0), PieceType.BISHOP, PieceColor.BLACK));
        board.Add(new Vector2Int(3, 0), factory.Get((3,0), PieceType.KING, PieceColor.BLACK));
        board.Add(new Vector2Int(4, 0), factory.Get((4,0), PieceType.QUEEN, PieceColor.BLACK));
        board.Add(new Vector2Int(5, 0), factory.Get((5,0), PieceType.BISHOP, PieceColor.BLACK));
        board.Add(new Vector2Int(6, 0), factory.Get((6,0), PieceType.KNIGHT, PieceColor.BLACK));
        board.Add(new Vector2Int(7, 0), factory.Get((7,0), PieceType.ROOK, PieceColor.BLACK));

        board.Add(new Vector2Int(0, 7), factory.Get((0,7), PieceType.ROOK, PieceColor.WHITE));
        board.Add(new Vector2Int(1, 7), factory.Get((1,7), PieceType.KNIGHT, PieceColor.WHITE));
        board.Add(new Vector2Int(2, 7), factory.Get((2,7), PieceType.BISHOP, PieceColor.WHITE));
        board.Add(new Vector2Int(3, 7), factory.Get((3,7), PieceType.KING, PieceColor.WHITE));
        board.Add(new Vector2Int(4, 7), factory.Get((4,7), PieceType.QUEEN, PieceColor.WHITE));
        board.Add(new Vector2Int(5, 7), factory.Get((5,7), PieceType.BISHOP, PieceColor.WHITE));
        board.Add(new Vector2Int(6, 7), factory.Get((6,7), PieceType.KNIGHT, PieceColor.WHITE));
        board.Add(new Vector2Int(7, 7), factory.Get((7,7), PieceType.ROOK, PieceColor.WHITE));

        for(int i = 0; i < 8; i++)
        {
            board.Add(new Vector2Int(i, 1), factory.Get((i,1), PieceType.PAWN, PieceColor.BLACK));
            board.Add(new Vector2Int(i, 6), factory.Get((i,6), PieceType.PAWN, PieceColor.WHITE));
        }
    }

    private void InitBoardSquares()
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                squares[x, y] = Instantiate(squarePrefab,new Vector3(1*x,0.03f,1*y),Quaternion.Euler(-90,0,0),transform);
                squares[x, y].position = new Vector2Int(x, y);
            }
        }
    }

    public void MovePiece(Vector2Int from, Vector2Int to)
    {
        pieceSelected.transform.position = squares[to.x, to.y].gameObject.transform.position; 
        board.Remove(from);
        if (board.ContainsKey(to))
        {
            board[to].gameObject.SetActive(false); 
            board.Remove(to);
        }
        board.Add(to, pieceSelected);
        pieceSelected.UpdatePosition(to);
    }

    public bool Occupied(Vector2Int position, out PieceColor color)
    {
        bool rv = board.TryGetValue(position, out PieceBehaviour tmp);
        color = rv ? tmp.color : PieceColor.WHITE;
        return rv;
    }

    public void HighlightPossibleMoves()
    {
        HidePossibleMoves();
        var possMoves = pieceSelected.PossibleMoves();
        foreach (Vector2Int pos in possMoves)
        {
            squares[pos.x, pos.y].gameObject.GetComponent<Renderer>().enabled=true;
        }
    }

    public void HidePossibleMoves()
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                squares[x, y].gameObject.GetComponent<Renderer>().enabled = false;
            }
        }
    }

    public void SetCurrentColliding(SquareBehaviour square)
    {
        currentCollidingSquare = square.position;
        Debug.Log("CurrentColliding = " + currentCollidingSquare);
        if(game.state == GameBehaviour.GameState.PICK || game.state == GameBehaviour.GameState.IDLE)
        {
            PieceBehaviour tmp;
            if (board.TryGetValue(currentCollidingSquare, out tmp) )
                pieceSelected = tmp;
            else
                Debug.LogError("No Piece In this Square ???? " + square.position);
        }
    }

    public bool checkMate(out PieceColor winner)
    {
        bool[] check = new bool[2];
        check[0] = false;
        check[1] = false;
        //for each color
        for(int c = 0; c < 2; c++)
        {
            List<Vector2Int> allMoves = new List<Vector2Int>();
            List<PieceBehaviour> colorPiece = new List<PieceBehaviour>();
            PieceBehaviour ennemiKing = null;
            foreach(PieceBehaviour piece in board.Values)
            {
                if(piece.color == (c == 0 ? PieceColor.WHITE : PieceColor.BLACK) )
                {
                    colorPiece.Add(piece);

                }
                else
                {
                    if(piece is KingBehaviour)
                    {
                        ennemiKing = piece;
                    }
                }
            }

            if(ennemiKing == null)
            {
                Debug.Log("No King");
                winner = (c == 0 ? PieceColor.WHITE : PieceColor.BLACK);
                return true;
            }

            foreach(PieceBehaviour pieceOfColor in colorPiece)
            {
                allMoves.AddRange(pieceOfColor.PossibleMoves());
            }
            List<Vector2Int> kingMoves = ennemiKing.PossibleMoves();
            kingMoves.Add(ennemiKing.position);
            List<Vector2Int> resultingMoves = new List<Vector2Int>();
            foreach(Vector2Int move in kingMoves)
            {
                Debug.Log("Moves of the king" + move);
                if(!allMoves.Contains(move))
                {
                    resultingMoves.Add(move);
                }
            }
            Debug.Log("Nb Moves of the king after = " + resultingMoves.Count);

            check[c] = (resultingMoves.Count == 0);
        }
        winner = (check[0] ? PieceColor.WHITE : PieceColor.BLACK);
        return (check[0] || check[1]);
    }
}
