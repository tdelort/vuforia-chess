﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RookBehaviour : PieceBehaviour
{

    public static List<Vector2Int> RookMoves(Vector2Int from, PieceColor color, BoardBehaviour board)
    {
        var moves = new List<Vector2Int>();
        // →
        for(int i = from.x + 1; i < 8; i++)
        {
            Vector2Int queryPosition = new Vector2Int(i, from.y);
            bool oc = board.Occupied(queryPosition, out PieceColor otherColor);
            if(!oc)
            {
                moves.Add(queryPosition);
            }
            else
            {
                if (otherColor != color)
                    moves.Add(queryPosition);
                break;
            }
        }
        // ←
        for(int i = from.x - 1; i >= 0; i--)
        {
            Vector2Int queryPosition = new Vector2Int(i, from.y);
            bool oc = board.Occupied(queryPosition, out PieceColor otherColor);
            if(!oc)
            {
                moves.Add(queryPosition);
            }
            else
            {
                if (otherColor != color)
                    moves.Add(queryPosition);
                break;
            }
        }

        // ↑
        for(int i = from.y + 1; i < 8; i++)
        {
            Vector2Int queryPosition = new Vector2Int(from.x,i);
            bool oc = board.Occupied(queryPosition, out PieceColor otherColor);
            if(!oc)
            {
                moves.Add(queryPosition);
            }
            else
            {
                if (otherColor != color)
                    moves.Add(queryPosition);
                break;
            }
        }
        // ↓
        for(int i = from.y - 1; i >= 0; i--)
        {
            Vector2Int queryPosition = new Vector2Int(from.x,i);
            bool oc = board.Occupied(queryPosition, out PieceColor otherColor);
            if(!oc)
            {
                moves.Add(queryPosition);
            }
            else
            {
                if (otherColor != color)
                    moves.Add(queryPosition);
                break;
            }
        }
        return moves;
    }

    public override List<Vector2Int> PossibleMoves()
    {
        return RookMoves(position, color, board);
    }
}
