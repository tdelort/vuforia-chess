﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameBehaviour : MonoBehaviour
{
    // A state machine controlling the game
    //
    // States : 
    // IDLE : Waiting for the player p to pick a piece
    //      switch to PICK if Entering a grabing Collider
    // PICK : The controller inside a grabing collider
    //      switch to IDLE if leaving collider before pickingTime
    //      switch to MOVE if leaving collider after  pickingTime
    // MOVE : Waiting for the player p to drop a piece
    //      switch to DROP if Entering a droping Collider
    // DROP : The controller inside a droping collider
    //      switch to MOVE if leaving collider before pickingTime
    //      switch to IDLE if leaving collider after  pickingTime, changing p
    //      switch to END if there is check mate
    // END : Check Mate, the game is over
    //
    // Colliders : 
    //      entering IDLE, colliders on piece that can move are set Active
    //      entering IDLE, dropping colliders are set Inactive
    //      entering MOVE, colliders on spot where the piece can move are set Active
    //      entering MOVE, grabbing colliders are set Inactive

    public enum GameState {
        IDLE,
        PICK,
        MOVE,
        DROP,
        END
    }

    public GameState state { get; set; } = GameState.IDLE;
    PieceColor p = PieceColor.BLACK; // whites are first (the color will be swapped at the beggining)
    public bool changedCollider { get; set; } = false;

    const float pickingTime = 1.5f;
    float pickingTimer = 0f;

    const float dropingTime = 1.5f;
    float dropingTimer = 0f;

    //triggers
    bool enteringIdle = true; //games starts by doing this
    bool enteringMove = false;

    PieceColor winner;

    //links
    BoardBehaviour board;
    GameObject hand;
    [SerializeField] Material whiteMaterial;
    [SerializeField] Material blackMaterial;
    [SerializeField] Image timer;
    [SerializeField] Canvas optionsCanvas;
    [SerializeField] Text winnerText;

    private void Start()
    {
        board = GameObject.FindObjectOfType<BoardBehaviour>();
        hand = GameObject.Find("Cursor");
    }

    void Update()
    {
        updateGUI();
        if(changedCollider)
        {
            // reset pick/drop timer
            pickingTimer = 0f;
            dropingTimer = 0f;
            changedCollider = false;
        }

        switch (state)
        {
            case GameState.IDLE:
                if(enteringIdle)
                {
                    enteringIdle = false;
                    enteringMove = true; //next time on Move, colliders will be reset

                    // change current player
                    // and change Hand color depending on current player
                    p = (p == PieceColor.WHITE ? PieceColor.BLACK : PieceColor.WHITE);
                    //hand.GetComponent<MeshRenderer>().material = (p == PieceColor.WHITE ? whiteMaterial : blackMaterial);

                    // set active false all colliders of board.squares
                    disableAllColliders();
                    // then, set active true all colliders on squares where there is a piece that can move
                    for (int x = 0; x < 8; x++)
                    {
                        for (int y = 0; y < 8; y++)
                        {
                            if (board.board.TryGetValue(new Vector2Int(x, y), out PieceBehaviour piece))
                            {
                                if (piece.color == p)
                                {
                                    var moves = piece.PossibleMoves();
                                    if (moves.Count > 0)
                                    {
                                        board.squares[piece.position.x, piece.position.y].gameObject.SetActive(true);
                                        board.squares[piece.position.x, piece.position.y].gameObject.GetComponent<Renderer>().enabled = true; //can be disabled
                                    }
                                }
                            }
                        }
                    }
                    if(board.checkMate(out winner))
                    {
                        state = GameState.END;

                    }
                }
                pickingTimer = 0f;
                dropingTimer = 0f;
                break;
            case GameState.PICK:
                pickingTimer += Time.deltaTime;
                //Debug.Log("Picking Timer = " + pickingTimer);
                if(pickingTimer > pickingTime)
                {
                    state = GameState.MOVE;
                    // Put current piece in hand
                    Debug.Log("Piece Selected : " + board.pieceSelected);
                    board.pieceSelected.transform.position = hand.transform.position;
                    board.pieceSelected.transform.parent = hand.transform;
                }
                break;

            case GameState.MOVE:
                if(enteringMove)
                {
                    enteringMove = false;
                    enteringIdle = true; //next time on Idle, colliders will be reset
                    // set active false all colliders
                    disableAllColliders();
                    // then, set active true all colliders on squares where the current piece can move
                    var possMoves = board.pieceSelected.PossibleMoves();
                    foreach(Vector2Int pos in possMoves)
                    {
                        board.squares[pos.x, pos.y].gameObject.SetActive(true);
                        board.HighlightPossibleMoves();
                    }
                }
                pickingTimer = 0f;
                dropingTimer = 0f;
                break;
            case GameState.DROP:
                dropingTimer += Time.deltaTime;
                Debug.Log("Droping Timer = " + dropingTimer);
                if(dropingTimer > dropingTime)
                {
                    state = GameState.IDLE;
                    // Put piece in hand in the slot
                    board.MovePiece(board.pieceSelected.position,board.currentCollidingSquare);
                    board.pieceSelected.transform.parent = board.transform;
                    board.pieceSelected.transform.rotation = Quaternion.Euler(Vector3.zero);
                }
                break;

            case GameState.END:
                optionsCanvas.gameObject.SetActive(true);
                winnerText.text = "Ckeckmate ! " + (winner == PieceColor.BLACK ? "Black " : "White ") + "wins !";
                winnerText.gameObject.SetActive(true);
                break;
            default:
                break;
        }
    }

    void disableAllColliders()
    {
        for (int x = 0; x < 8; x++)
        {
            for (int y = 0; y < 8; y++)
            {
                board.squares[x, y].gameObject.SetActive(false);
                board.squares[x, y].GetComponent<Renderer>().material = board.squares[x, y].defaultMaterial;
            }
        }
    }

    void updateGUI()
    {
        timer.fillAmount = Mathf.Max(dropingTimer/dropingTime,pickingTimer/pickingTime);
    }

    public void restartGame()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    public void quit()
    {
        Application.Quit(); 
    }

}
