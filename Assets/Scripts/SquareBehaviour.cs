﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquareBehaviour : MonoBehaviour
{
    GameBehaviour game;
    BoardBehaviour board;
    public Vector2Int position;

    [SerializeField] public Material defaultMaterial;
    [SerializeField] public Material secondaryMaterial;

    private void Start()
    {
        game = FindObjectOfType<GameBehaviour>();
        board = FindObjectOfType<BoardBehaviour>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Hand"))
        {
            board.SetCurrentColliding(this);
            gameObject.GetComponent<Renderer>().material = secondaryMaterial;
            if (game.state == GameBehaviour.GameState.IDLE)
            { game.state = GameBehaviour.GameState.PICK; }
            else
            if (game.state == GameBehaviour.GameState.MOVE)
            { game.state = GameBehaviour.GameState.DROP; }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Hand"))
        {
            game.changedCollider = true;
            gameObject.GetComponent<Renderer>().material = defaultMaterial;
            if (game.state == GameBehaviour.GameState.PICK)
            { game.state = GameBehaviour.GameState.IDLE; }
            else
            if (game.state == GameBehaviour.GameState.DROP)
            { game.state = GameBehaviour.GameState.MOVE; }
        }
    }


}
